import React, { Component } from 'react';
import { View } from 'react-native';
import firebase from 'firebase';
import { Header, Button, Spinner } from './components/common';
import LoginForm from './components/LoginForm';

class App extends Component {

    state ={ loggedIn: null };

    componentWillMount(){
        firebase.initializeApp({
            apiKey: 'AIzaSyBPntO7SgY3jXY6CcBM-h2jWpsdhLlTrGA',
            authDomain: 'auth-c353f.firebaseapp.com',
            databaseURL: 'https://auth-c353f.firebaseio.com',
            projectId: 'auth-c353f',
            storageBucket: 'auth-c353f.appspot.com',
            messagingSenderId: '386932834638'
        });


        firebase.auth().onAuthStateChanged((user) => {
            if(user){
                this.setState({loggedIn: true})
            } else {
                this.setState({loggedIn: false})
            }
        });
    }

    renderContent(){


        switch(this.state.loggedIn){
            case true:
                return (
                    <Button onPress={() => firebase.auth().signOut()}>
                        Log Out
                    </Button>
                );
            case false:
                return <LoginForm />
            default:
                return <Spinner size="large" />
        }
        
    }

    render(){
        return (
            <View>
                <Header headerText="Authentication" />
                {this.renderContent()}
            </View>
        );
    }
}

export default App;